package com.mingyue.pekostudio.tennisstats.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.mingyue.pekostudio.tennisstats.R;
import com.mingyue.pekostudio.tennisstats.adapter.ChartListViewAdapter;
import com.mingyue.pekostudio.tennisstats.adapter.SavedGameListViewAdapter;
import com.mingyue.pekostudio.tennisstats.base.CommonActivity;

import java.util.List;

import static com.mingyue.pekostudio.tennisstats.commons.Constants.GAME;

public class ChartActivity extends CommonActivity implements View.OnClickListener{

    ListView lst_chart;
    ChartListViewAdapter _adpater;

    TextView txv_header_place, txv_header_surface, txv_player1_name,txv_player2_name,txv_play1_ace, txv_play2_ace, txv_play1_1st_service_pro, txv_play1_1st_service_num, txv_play2_1st_service_pro, txv_play2_1st_service_num ,
            txv_play1_wfault, txv_play2_wfault, txv_play1_1st_win_pro, txv_play1_1st_win_num, txv_play2_1st_win_pro, txv_play2_1st_win_num, txv_play1_2nd_win_pro, txv_play1_2nd_win_num, txv_play2_2nd_win_pro, txv_play2_2nd_win_num ,
            txv_play1_total_point, txv_play2_total_point, txv_play1_receiving_pro,txv_play1_receiving_num, txv_play2_receiving_pro, txv_play2_receiving_num;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);

        loadLayout();
    }

    private void loadLayout() {

        lst_chart = (ListView)findViewById(R.id.lst_chart);
        LayoutInflater myinflater = getLayoutInflater();
         ViewGroup myHeader = (ViewGroup)myinflater.inflate(R.layout.chart_header, lst_chart, false);
        lst_chart.addHeaderView(myHeader, null, false);

        _adpater = new ChartListViewAdapter(this);
        lst_chart.setAdapter(_adpater);

        txv_header_place =(TextView) myHeader.findViewById(R.id.txv_header_place);
        txv_header_surface =(TextView) myHeader.findViewById(R.id.txv_header_surface);
        txv_player1_name =(TextView) myHeader.findViewById(R.id.txv_player1_name);
        txv_player2_name =(TextView) myHeader.findViewById(R.id.txv_player2_name);

        txv_play1_ace =(TextView) myHeader.findViewById(R.id.txv_play1_ace);
        txv_play2_ace =(TextView) myHeader.findViewById(R.id.txv_play2_ace);

        txv_play1_1st_service_pro =(TextView) myHeader.findViewById(R.id.txv_play1_1st_service_pro);
        txv_play1_1st_service_num =(TextView) myHeader.findViewById(R.id.txv_play1_1st_service_num);

        txv_play2_1st_service_pro =(TextView) myHeader.findViewById(R.id.txv_play2_1st_service_pro);
        txv_play2_1st_service_num =(TextView) myHeader.findViewById(R.id.txv_play2_1st_service_num);

        txv_play1_wfault =(TextView) myHeader.findViewById(R.id.txv_play1_wfault);
        txv_play2_wfault =(TextView) myHeader.findViewById(R.id.txv_play2_wfault);

        txv_play1_1st_win_pro =(TextView) myHeader.findViewById(R.id.txv_play1_1st_win_pro);
        txv_play1_1st_win_num =(TextView) myHeader.findViewById(R.id.txv_play1_1st_win_num);
        txv_play2_1st_win_pro =(TextView) myHeader.findViewById(R.id.txv_play2_1st_win_pro);
        txv_play2_1st_win_num =(TextView) myHeader.findViewById(R.id.txv_play2_1st_win_num);

        txv_play1_2nd_win_pro =(TextView) myHeader.findViewById(R.id.txv_play1_2nd_win_pro);
        txv_play1_2nd_win_num =(TextView) myHeader.findViewById(R.id.txv_play1_2nd_win_num);
        txv_play2_2nd_win_pro =(TextView) myHeader.findViewById(R.id.txv_play2_2nd_win_pro);
        txv_play2_2nd_win_num =(TextView) myHeader.findViewById(R.id.txv_play2_2nd_win_num);

        txv_play1_total_point =(TextView) myHeader.findViewById(R.id.txv_play1_total_point);
        txv_play2_total_point =(TextView) myHeader.findViewById(R.id.txv_play2_total_point);

        txv_play1_receiving_pro =(TextView) myHeader.findViewById(R.id.txv_play1_receiving_pro);
        txv_play1_receiving_num =(TextView) myHeader.findViewById(R.id.txv_play1_receiving_num);
        txv_play2_receiving_pro =(TextView) myHeader.findViewById(R.id.txv_play2_receiving_pro);
        txv_play2_receiving_num =(TextView) myHeader.findViewById(R.id.txv_play2_receiving_num);

    }

    @Override
    public void onClick(View v) {

    }

    private void gotoMain(){

        startActivity(new Intent(ChartActivity.this, MainActivity.class));

        overridePendingTransition(0,0);
        finish();
    }

    @Override
    public void onBackPressed() {
        gotoMain();
    }
}
