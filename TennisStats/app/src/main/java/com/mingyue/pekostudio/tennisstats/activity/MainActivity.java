package com.mingyue.pekostudio.tennisstats.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mingyue.pekostudio.tennisstats.R;
import com.mingyue.pekostudio.tennisstats.base.CommonActivity;
import com.mingyue.pekostudio.tennisstats.commons.Constants;

public class MainActivity extends CommonActivity implements View.OnClickListener {

    DrawerLayout ui_drawerlayout;

    //left & right score & function buttons
    TextView txv_first_player_name, txv_score_left, txv_score_right, txv_second_player_name, txv_fault_left, txv_fault_right;
    TextView txv_service_fault_left, txv_service_ace_left, txv_service_left, txv_winner_left, txv_forced_error_left, txv_unforced_error_left;
    TextView txv_service_fault_right, txv_service_ace_right, txv_service_right, txv_winner_right, txv_forced_error_right, txv_unforced_error_right;
    ImageView imv_change, imv_ball_left, imv_ball_right;

    //score table left
    LinearLayout lyt_score_first_left,lyt_score_sec_left,lyt_score_third_left,lyt_score_fourth_left,lyt_score_fifth_left;
    TextView txv_score_first_side_left, txv_score_second_side_left, txv_score_third_side_left, txv_score_fourth_side_left, txv_score_fifth_side_left;
    TextView txv_score_first_left, txv_score_second_left, txv_score_third_left, txv_score_fourth_left,txv_score_fifth_left ;

    //score table right
    LinearLayout lyt_score_first_right,lyt_score_sec_right,lyt_score_third_right,lyt_score_fourth_right,lyt_score_fifth_right;
    TextView txv_score_first_side_right, txv_score_second_side_right, txv_score_third_side_right, txv_score_fourth_side_right, txv_score_fifth_side_right;
    TextView txv_score_first_right, txv_score_second_right, txv_score_third_right, txv_score_fourth_right,txv_score_fifth_right ;

    //tool bar
    ImageView imv_new, imv_download, imv_upload, imv_setting, imv_back, imv_record, imv_chart;

    //left navigation bar
    ImageView imv_nav_back_left;
    TextView txv_nav_fore_left, txv_nav_back_left, txv_nav_drive_left, txv_nav_volley_left, txv_nav_smash_left, txv_nav_drop_left;

    //right navigation bar
    ImageView imv_nav_back_right;
    TextView txv_nav_fore_right, txv_nav_back_right, txv_nav_drive_right, txv_nav_volley_right, txv_nav_smash_right, txv_nav_drop_right;

    //player1 name, player2 name
    String player1_name = "", player2_name = "";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadLayout();
    }

    private void loadLayout() {

        ui_drawerlayout = (DrawerLayout)findViewById(R.id.drawerlayout);

        //name & scores
        txv_first_player_name = (TextView)findViewById(R.id.txv_first_player_name);
        txv_second_player_name = (TextView)findViewById(R.id.txv_second_player_name);

        txv_score_left = (TextView)findViewById(R.id.txv_score_left);
        txv_score_left.setOnClickListener(this);

        txv_score_right = (TextView)findViewById(R.id.txv_score_right);
        txv_score_right.setOnClickListener(this);

        txv_fault_left = (TextView)findViewById(R.id.txv_fault_left);
        txv_fault_left.setOnClickListener(this);

        txv_fault_right = (TextView)findViewById(R.id.txv_fault_right);
        txv_fault_right.setOnClickListener(this);

        imv_ball_left = (ImageView)findViewById(R.id.imv_ball_left);
        imv_ball_left.setOnClickListener(this);

        imv_ball_right = (ImageView)findViewById(R.id.imv_ball_right);
        imv_ball_right.setOnClickListener(this);


        imv_change = (ImageView)findViewById(R.id.imv_change);
        imv_change.setOnClickListener(this);

        //function buttons

        /*left*/
        txv_service_fault_left = (TextView)findViewById(R.id.txv_service_fault_left);
        txv_service_fault_left.setOnClickListener(this);

        txv_service_ace_left = (TextView)findViewById(R.id.txv_service_ace_left);
        txv_service_ace_left.setOnClickListener(this);

        txv_service_left = (TextView)findViewById(R.id.txv_service_left);
        txv_service_left.setOnClickListener(this);

        txv_winner_left = (TextView)findViewById(R.id.txv_winner_left);
        txv_winner_left.setOnClickListener(this);

        txv_forced_error_left = (TextView)findViewById(R.id.txv_forced_error_left);
        txv_forced_error_left.setOnClickListener(this);

        txv_unforced_error_left = (TextView)findViewById(R.id.txv_unforced_error_left);
        txv_unforced_error_left.setOnClickListener(this);

        /*right*/
        txv_service_fault_right = (TextView)findViewById(R.id.txv_service_fault_right);
        txv_service_fault_right.setOnClickListener(this);

        txv_service_ace_right = (TextView)findViewById(R.id.txv_service_ace_right);
        txv_service_ace_right.setOnClickListener(this);

        txv_service_right = (TextView)findViewById(R.id.txv_service_right);
        txv_service_right.setOnClickListener(this);

        txv_winner_right = (TextView)findViewById(R.id.txv_winner_right);
        txv_winner_right.setOnClickListener(this);

        txv_forced_error_right = (TextView)findViewById(R.id.txv_forced_error_right);
        txv_forced_error_right.setOnClickListener(this);

        txv_unforced_error_right = (TextView)findViewById(R.id.txv_unforced_error_right);
        txv_unforced_error_right.setOnClickListener(this);

        //score table

        /*left linear layout*/
        lyt_score_first_left =(LinearLayout)findViewById(R.id.lyt_score_first_left);
        lyt_score_sec_left =(LinearLayout)findViewById(R.id.lyt_score_second_left);
        lyt_score_third_left =(LinearLayout)findViewById(R.id.lyt_score_third_left);
        lyt_score_fourth_left =(LinearLayout)findViewById(R.id.lyt_score_fourth_left);
        lyt_score_fifth_left =(LinearLayout)findViewById(R.id.lyt_score_fifth_left);

        /*left side score*/
        txv_score_first_side_left = (TextView)findViewById(R.id.txv_score_first_side_left);
        txv_score_second_side_left = (TextView)findViewById(R.id.txv_score_second_side_left);
        txv_score_third_side_left = (TextView)findViewById(R.id.txv_score_third_side_left);
        txv_score_fourth_side_left = (TextView)findViewById(R.id.txv_score_fourth_side_left);
        txv_score_fifth_side_left = (TextView)findViewById(R.id.txv_score_fifth_side_left);
        /*scores*/
        txv_score_first_left = (TextView)findViewById(R.id.txv_score_first_left);
        txv_score_second_left = (TextView)findViewById(R.id.txv_score_second_left);
        txv_score_third_left = (TextView)findViewById(R.id.txv_score_third_left);
        txv_score_fourth_left = (TextView)findViewById(R.id.txv_score_fourth_left);
        txv_score_fifth_left = (TextView)findViewById(R.id.txv_score_fifth_left);


        /*right linear layout*/
        lyt_score_first_right =(LinearLayout)findViewById(R.id.lyt_score_first_right);
        lyt_score_sec_right =(LinearLayout)findViewById(R.id.lyt_score_second_right);
        lyt_score_third_right =(LinearLayout)findViewById(R.id.lyt_score_third_right);
        lyt_score_fourth_right =(LinearLayout)findViewById(R.id.lyt_score_fourth_right);
        lyt_score_fifth_right =(LinearLayout)findViewById(R.id.lyt_score_fifth_right);

        /*right side score*/
        txv_score_first_side_right = (TextView)findViewById(R.id.txv_score_first_side_right);
        txv_score_second_side_right = (TextView)findViewById(R.id.txv_score_second_side_right);
        txv_score_third_side_right = (TextView)findViewById(R.id.txv_score_third_side_right);
        txv_score_fourth_side_right = (TextView)findViewById(R.id.txv_score_fourth_side_right);
        txv_score_fifth_side_right = (TextView)findViewById(R.id.txv_score_fifth_side_right);
        /*scores*/
        txv_score_first_right = (TextView)findViewById(R.id.txv_score_first_right);
        txv_score_second_right = (TextView)findViewById(R.id.txv_score_second_right);
        txv_score_third_right = (TextView)findViewById(R.id.txv_score_third_right);
        txv_score_fourth_right = (TextView)findViewById(R.id.txv_score_fourth_right);
        txv_score_fifth_right = (TextView)findViewById(R.id.txv_score_fifth_right);

        //toolbar
        imv_new = (ImageView)findViewById(R.id.imv_new);
        imv_new.setOnClickListener(this);

        imv_download = (ImageView)findViewById(R.id.imv_download);
        imv_download.setOnClickListener(this);

        imv_upload = (ImageView)findViewById(R.id.imv_upload);
        imv_upload.setOnClickListener(this);

        imv_setting = (ImageView)findViewById(R.id.imv_setting);
        imv_setting.setOnClickListener(this);

        imv_back = (ImageView)findViewById(R.id.imv_back);
        imv_back.setOnClickListener(this);

        imv_record = (ImageView)findViewById(R.id.imv_record);
        imv_record.setOnClickListener(this);

        imv_chart = (ImageView)findViewById(R.id.imv_chart);
        imv_chart.setOnClickListener(this);

        //left navigation

        imv_nav_back_left = (ImageView)findViewById(R.id.imv_nav_back_left);
        imv_nav_back_left.setOnClickListener(this);

        txv_nav_fore_left = (TextView)findViewById(R.id.txv_nav_fore_left);
        txv_nav_fore_left.setOnClickListener(this);

        txv_nav_back_left = (TextView)findViewById(R.id.txv_nav_back_left);
        txv_nav_fore_left.setOnClickListener(this);

        txv_nav_drive_left = (TextView)findViewById(R.id.txv_nav_drive_left);
        txv_nav_drive_left.setOnClickListener(this);

        txv_nav_volley_left = (TextView)findViewById(R.id.txv_nav_volley_left);
        txv_nav_volley_left.setOnClickListener(this);

        txv_nav_smash_left = (TextView)findViewById(R.id.txv_nav_smash_left);
        txv_nav_smash_left.setOnClickListener(this);

        txv_nav_drop_left = (TextView)findViewById(R.id.txv_nav_drop_left);
        txv_nav_drop_left.setOnClickListener(this);

        //right navigation

        imv_nav_back_right = (ImageView)findViewById(R.id.imv_nav_back_right);
        imv_nav_back_right.setOnClickListener(this);

        txv_nav_fore_right = (TextView)findViewById(R.id.txv_nav_fore_right);
        txv_nav_fore_right.setOnClickListener(this);

        txv_nav_back_right = (TextView)findViewById(R.id.txv_nav_back_right);
        txv_nav_fore_left.setOnClickListener(this);

        txv_nav_drive_right = (TextView)findViewById(R.id.txv_nav_drive_right);
        txv_nav_drive_right.setOnClickListener(this);

        txv_nav_volley_right = (TextView)findViewById(R.id.txv_nav_volley_right);
        txv_nav_volley_right.setOnClickListener(this);

        txv_nav_smash_right = (TextView)findViewById(R.id.txv_nav_smash_right);
        txv_nav_smash_right.setOnClickListener(this);

        txv_nav_drop_right = (TextView)findViewById(R.id.txv_nav_drop_right);
        txv_nav_drop_right.setOnClickListener(this);

        //ball show setting

        if (Constants.BALL_STATE == 1){
            imv_ball_left.setVisibility(View.VISIBLE);
            imv_ball_right.setVisibility(View.GONE);
        } else {

            imv_ball_left.setVisibility(View.GONE);
            imv_ball_right.setVisibility(View.VISIBLE);
        }



    }

    public void showDrawer_Left() {

        DrawerLayout ui_drawerlayout =(DrawerLayout)findViewById(R.id.drawerlayout);

        ui_drawerlayout.openDrawer(Gravity.LEFT);
    }

    public void showDrawer_Right() {

        DrawerLayout ui_drawerlayout =(DrawerLayout)findViewById(R.id.drawerlayout);

        ui_drawerlayout.openDrawer(Gravity.RIGHT);
    }


    public void showAlertDialog_new(String msg) {

        AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        alertDialog.setTitle("Create new score");
        alertDialog.setMessage(msg);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),

                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        showToast("Okay");

                    }
                });

        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.cancel),

                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        showToast("Cancel");

                    }
                });

        //alertDialog.setIcon(R.drawable.banner);
        alertDialog.show();

    }

    public void showAlertDialog_Save(String msg) {

        AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        alertDialog.setTitle("Save?");
        alertDialog.setMessage(msg);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),

                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        showToast("Okay");

                    }
                });

        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.cancel),

                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        showToast("Cancel");

                    }
                });

        //alertDialog.setIcon(R.drawable.banner);
        alertDialog.show();

    }

    public void showAlertDialog_Back(String msg) {

        AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        alertDialog.setTitle("Save?");
        alertDialog.setMessage(msg);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),

                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        showToast("Okay");

                    }
                });

        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.cancel),

                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        showToast("Cancel");

                    }
                });

        //alertDialog.setIcon(R.drawable.banner);
        alertDialog.show();

    }

    private void gotoSavedGameActivity(){

        startActivity(new Intent(this, SavedGameActivity.class));

        overridePendingTransition(0,0);
        finish();
    }

    private void gotoSettingActity(){

        startActivity(new Intent(this, SettingActivity.class));
        overridePendingTransition(0,0);

        finish();
    }

    private void gotoRecordActivity(){

        startActivity(new Intent(this, RecordedActivity.class));
        overridePendingTransition(0,0);

        finish();
    }

    private void gotoChartActivity(){

        startActivity(new Intent(this, ChartActivity.class));
        overridePendingTransition(0,0);

        finish();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_change:

                break;

            case R.id.imv_ball_left:

                break;

            case R.id.imv_ball_right:

                break;

            case R.id.txv_score_left:

                break;

            case R.id.txv_score_right:

                break;

            case R.id.txv_fault_left:


                break;

            case R.id.txv_fault_right:

                break;


            /*left navigation*//*===========================================left navigation================================================*/
            case R.id.imv_nav_back_left:

                ui_drawerlayout.closeDrawers();

                break;

            case R.id.txv_nav_fore_left:

                ui_drawerlayout.closeDrawers();
                break;

            case R.id.txv_nav_back_left:

                ui_drawerlayout.closeDrawers();
                break;

            case R.id.txv_nav_drive_left:

                ui_drawerlayout.closeDrawers();
                break;

            case R.id.txv_nav_volley_left:

                ui_drawerlayout.closeDrawers();
                break;

            case R.id.txv_nav_smash_left:

                ui_drawerlayout.closeDrawers();
                break;

            case R.id.txv_nav_drop_left:

                ui_drawerlayout.closeDrawers();
                break;

            /*left function buttons*//*========================================left function buttons=========================================================*/
            case R.id.txv_service_fault_left:

                showDrawer_Left();
                break;

            case R.id.txv_service_ace_left:
                showDrawer_Left();
                break;

            case R.id.txv_service_left:
                showDrawer_Left();
                break;

            case R.id.txv_winner_left:

                showDrawer_Left();
                break;

            case R.id.txv_forced_error_left:

                showDrawer_Left();
                break;

            case R.id.txv_unforced_error_left:

                showDrawer_Left();
                break;


            /*right navigation*//******************************************************************Right Navigation buttons*********************************************************************/
            case R.id.imv_nav_back_right:
                ui_drawerlayout.closeDrawers();
                break;
            case R.id.txv_nav_fore_right:
                ui_drawerlayout.closeDrawers();
                break;

            case R.id.txv_nav_back_right:
                ui_drawerlayout.closeDrawers();
                break;

            case R.id.txv_nav_drive_right:
                ui_drawerlayout.closeDrawers();

                break;

            case R.id.txv_nav_volley_right:
                ui_drawerlayout.closeDrawers();
                break;

            case R.id.txv_nav_smash_right:

                ui_drawerlayout.closeDrawers();
                break;

            case R.id.txv_nav_drop_right:

                ui_drawerlayout.closeDrawers();
                break;

            /*right buttons function*//*==============================================Right buttons function=========================================================================*/
            case R.id.txv_service_fault_right:
                showDrawer_Right();
                break;

            case R.id.txv_service_ace_right:
                showDrawer_Right();
                break;

            case R.id.txv_service_right:

                showDrawer_Right();
                break;

            case R.id.txv_winner_right:

                showDrawer_Right();
                break;

            case R.id.txv_forced_error_right:

                showDrawer_Right();
                break;

            case R.id.txv_unforced_error_right:

                showDrawer_Right();
                break;

            /*toolbar*/
            case R.id.imv_new:

                showAlertDialog_new("Delete current score?");

                break;

            case R.id.imv_download:

                showAlertDialog_Save(txv_first_player_name.getText().toString() + " " + "vs" + " " + txv_second_player_name.getText().toString());

                break;

            case R.id.imv_upload:

                gotoSavedGameActivity();

                break;

            case R.id.imv_setting:
                gotoSettingActity();
                break;

            case R.id.imv_back:
                showAlertDialog_Back("Back before a point");
                break;

            case R.id.imv_record:
                gotoRecordActivity();
                break;

            case R.id.imv_chart:
                gotoChartActivity();
                break;

        }

    }

    @Override
    public void onBackPressed() {
        showAlertDialog_Exit();
    }

    public void showAlertDialog_Exit() {

        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage("Do you want exit?");

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.ok),

                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        finish();

                    }
                });

        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.cancel),

                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        alertDialog.dismiss();
                    }
                });

        //alertDialog.setIcon(R.drawable.banner);
        alertDialog.show();

    }
}
