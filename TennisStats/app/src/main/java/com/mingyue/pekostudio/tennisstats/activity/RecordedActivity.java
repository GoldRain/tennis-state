package com.mingyue.pekostudio.tennisstats.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.mingyue.pekostudio.tennisstats.R;
import com.mingyue.pekostudio.tennisstats.base.CommonActivity;

import org.w3c.dom.Comment;

public class RecordedActivity extends CommonActivity implements View.OnClickListener {

    LinearLayout lyt_set1,lyt_set2, lyt_set3, lyt_set4, lyt_set5, lyt_set6;
    View v_set1, v_set2, v_set3, v_set4, v_set5,v_set6;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recorded);

        loadLayout();
    }

    private void loadLayout() {

        lyt_set1 = (LinearLayout)findViewById(R.id.lyt_set1);
        lyt_set1.setOnClickListener(this);

        lyt_set2 = (LinearLayout)findViewById(R.id.lyt_set2);
        lyt_set2.setOnClickListener(this);

        lyt_set3 = (LinearLayout)findViewById(R.id.lyt_set3);
        lyt_set3.setOnClickListener(this);

        lyt_set4 = (LinearLayout)findViewById(R.id.lyt_set4);
        lyt_set4.setOnClickListener(this);

        lyt_set5 = (LinearLayout)findViewById(R.id.lyt_set5);
        lyt_set5.setOnClickListener(this);

        v_set1 = (View)findViewById(R.id.v_set1);
        v_set2 = (View)findViewById(R.id.v_set2);
        v_set3 = (View)findViewById(R.id.v_set3);
        v_set4 = (View)findViewById(R.id.v_set4);
        v_set5 = (View)findViewById(R.id.v_set5);

        selectColor(View.VISIBLE, View.GONE, View.GONE, View.GONE, View.GONE);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.lyt_set1:
                selectColor(View.VISIBLE, View.GONE, View.GONE, View.GONE, View.GONE);
                break;

            case R.id.lyt_set2:
                selectColor(View.GONE, View.VISIBLE, View.GONE, View.GONE, View.GONE);
                break;

            case R.id.lyt_set3:
                selectColor(View.GONE, View.GONE, View.VISIBLE, View.GONE, View.GONE);
                break;

            case R.id.lyt_set4:
                selectColor(View.GONE, View.GONE, View.GONE, View.VISIBLE, View.GONE);
                break;

            case R.id.lyt_set5:
                selectColor(View.GONE, View.GONE, View.GONE, View.GONE, View.VISIBLE);
                break;
        }

    }

    private void selectColor(int a, int b, int c, int d, int e){
        v_set1.setVisibility(a); v_set2.setVisibility(b); v_set3.setVisibility(c); v_set4.setVisibility(d); v_set5.setVisibility(e);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(RecordedActivity.this, MainActivity.class));
        overridePendingTransition(0,0);
        finish();
    }
}
