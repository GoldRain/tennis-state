package com.mingyue.pekostudio.tennisstats.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mingyue.pekostudio.tennisstats.R;
import com.mingyue.pekostudio.tennisstats.activity.MainActivity;
import com.mingyue.pekostudio.tennisstats.activity.SavedGameActivity;
import com.mingyue.pekostudio.tennisstats.model.GameModel;

import java.util.ArrayList;

/**
 * Created by HugeRain on 6/23/2017.
 */

public class SavedGameListViewAdapter extends BaseAdapter {

    Context _context;
    ArrayList<GameModel> _allSavedGames = new ArrayList<>();
    SavedGameActivity _activity;

    public SavedGameListViewAdapter(Context activity, ArrayList<GameModel> _games ){

        _context = activity;
        this._allSavedGames = _games;

    }

    @Override
    public int getCount() {
        return _allSavedGames.size();
    }

    @Override
    public Object getItem(int position) {
        return _allSavedGames.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SavedGameHolder holder;

        if (convertView == null){

            holder = new SavedGameHolder();

            LayoutInflater inflater = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_list_saved_game, parent, false);

            holder.txv_names = (TextView)convertView.findViewById(R.id.txv_names);
            holder.txv_date = (TextView)convertView.findViewById(R.id.txv_date);

            holder.imv_mail = (ImageView)convertView.findViewById(R.id.imv_mail);
            holder.imv_delete = (ImageView)convertView.findViewById(R.id.imv_delete);

            convertView.setTag(holder);

        } else {

            holder = (SavedGameHolder)convertView.getTag();
        }

        final GameModel game  = _allSavedGames.get(position);

        holder.txv_names.setText(game.get_playerName1() + " " + "vs" + " " + game.get_playerName2());
        holder.txv_date.setText(game.get_time());

        holder.imv_mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sendMail();

            }
        });

        holder.imv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _allSavedGames.remove(game);
                notifyDataSetChanged();
            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _context.startActivity(new Intent(_context, MainActivity.class));
                /*_context.finish();*/
            }
        });

        return convertView;
    }

    private void sendMail() {

        /*_context.showProgress();*/

        String to = "";
        String subject = "";
        String message = "";

        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{to});
        email.putExtra(Intent.EXTRA_SUBJECT, subject);
        email.putExtra(Intent.EXTRA_TEXT, message);

        //need this to prompts email client only
        email.setType("message/rfc822");

        _context.startActivity(Intent.createChooser(email, "Choose an Email client :"));

        /*_activity.closeProgress();*/

    }

    public class SavedGameHolder{

        TextView txv_names;
        TextView txv_date;
        ImageView imv_mail, imv_delete;
    }
}
