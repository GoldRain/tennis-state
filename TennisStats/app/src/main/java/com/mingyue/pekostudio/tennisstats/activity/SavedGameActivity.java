package com.mingyue.pekostudio.tennisstats.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.mingyue.pekostudio.tennisstats.R;
import com.mingyue.pekostudio.tennisstats.adapter.SavedGameListViewAdapter;
import com.mingyue.pekostudio.tennisstats.base.CommonActivity;
import com.mingyue.pekostudio.tennisstats.model.GameModel;

import java.util.ArrayList;
import java.util.List;

import static com.mingyue.pekostudio.tennisstats.commons.Constants.GAME;

public class SavedGameActivity extends CommonActivity implements View.OnClickListener {

    ListView lst_saved_games;
    SavedGameListViewAdapter _adapter;
    ArrayList<GameModel> _allGames = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_game);

        loadLayout();
    }

    private void loadLayout() {

        ImageView imv_back = (ImageView)findViewById(R.id.imv_back);
        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gotoMain();
            }
        });

        for (int i = 0; i < 10; i++){

            GameModel game =  new GameModel();

            game.set_id(i);
            game.set_playerName1("Play1"+ " " + "Name" +  i );
            game.set_playerName2("Play2" + " " + "Name" +  i);
            game.set_time("wed Jun 21 2017 " + (i + 10)+ ":" + i + ":" + i*5 + " " + "GMT+0800(CST)" );

            _allGames.add(game);
        }

        GAME = _allGames;

        lst_saved_games = (ListView)findViewById(R.id.lst_saved_game);
        _adapter = new SavedGameListViewAdapter(this, _allGames);
        lst_saved_games.setAdapter(_adapter);

    }

    private void gotoMain(){

        startActivity(new Intent(SavedGameActivity.this, MainActivity.class));
        overridePendingTransition(0,0);
        finish();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onBackPressed() {
        gotoMain();
    }
}
