package com.mingyue.pekostudio.tennisstats.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mingyue.pekostudio.tennisstats.R;
import com.mingyue.pekostudio.tennisstats.activity.ChartActivity;

import java.util.ArrayList;

/**
 * Created by HugeRain on 6/25/2017.
 */

public class ChartListViewAdapter extends BaseAdapter {

    Context _context;
    ChartActivity _activity ;
    Object _charts = new Object();

    ArrayList<String> _game_lists = new ArrayList<>();

    public ChartListViewAdapter(ChartActivity activity){

        this._activity = activity;

        loadLists();
    }

    private void loadLists(){

        _game_lists.add("Service fault");_game_lists.add("Fore, Volley"); _game_lists.add("Drive");_game_lists.add("Volley");_game_lists.add("Smash");_game_lists.add("Drop Lob");
        _game_lists.add("Service ace");_game_lists.add("Fore, Volley"); _game_lists.add("Drive");_game_lists.add("Volley");_game_lists.add("Smash");_game_lists.add("Drop Lob");
        _game_lists.add("Winners");_game_lists.add("Fore, Volley"); _game_lists.add("Drive");_game_lists.add("Volley");_game_lists.add("Smash");_game_lists.add("Drop Lob");
    }

    @Override
    public int getCount() {
        return _game_lists.size();
    }

    @Override
    public Object getItem(int position) {
        return _game_lists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ChartHolder holder;

        if (convertView ==  null){

            holder = new ChartHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_list_chart, parent, false);

            holder.txv_title = (TextView)convertView.findViewById(R.id.txv_title);

            convertView.setTag(holder);

        } else {

            holder = (ChartHolder)convertView.getTag();
        }

        holder.txv_title.setText(_game_lists.get(position));
        String title = _game_lists.get(position);

        if (title.equals("Service fault")) /*holder.txv_title.setBackgroundColor(_activity.getResources().getColor(R.color.service_fault));*/
            convertView.setBackgroundColor(_activity.getResources().getColor(R.color.service_fault));

        if (position == 0) convertView.setBackgroundColor(_activity.getResources().getColor(R.color.service_fault));
        else if (position > 0 && position < 6) convertView.setBackgroundColor(_activity.getResources().getColor(R.color.service_fault_bg));
        else if (position == 6) convertView.setBackgroundColor(_activity.getResources().getColor(R.color.service_ace));
        else if (position > 6 && position < 12) convertView.setBackgroundColor(_activity.getResources().getColor(R.color.service_ace_bg));
        else if (position == 12) convertView.setBackgroundColor(_activity.getResources().getColor(R.color.winner));
        else if (position > 12) convertView.setBackgroundColor(_activity.getResources().getColor(R.color.winner_bg));

        return convertView;
    }

    public class ChartHolder {

        TextView txv_play1, txv_play2, txv_title;
    }
}
