package com.mingyue.pekostudio.tennisstats.commons;

import com.mingyue.pekostudio.tennisstats.model.GameModel;

import java.util.ArrayList;

/**
 * Created by HGS on 12/11/2015.
 */
public class Constants {

    public static final int VOLLEY_TIME_OUT = 60000;
    public static final int SPLASH_TIME = 2000;
    public static final int PROFILE_IMAGE_SIZE = 256;

    public static final int RECENT_MESSAGE_COUNT = 20;

    public static ArrayList<GameModel> GAME = new ArrayList<>();

    public static final int PICK_FROM_CAMERA = 100;
    public static final int PICK_FROM_ALBUM = 101;
    public static final int CROP_FROM_CAMERA = 102;

    public static final String USER = "user";
    public static final String CLASS = "class";
    public static final String KEY_ROOM = "room";

    public static final String XMPP_START = "xmpp";
    public static final int XMPP_FROMBROADCAST = 0;
    public static final int XMPP_FROMLOGIN = 1;

    public static final String KEY_SEPERATOR = "#";
    public static final String KEY_ROOM_MARKER = "ROOM#";

    public static final String KEY_LOGOUT = "logout";

    public static final int NORMAL_NOTI_ID = 1;

    public static final String KEY_IMAGEPATH = "image_path";
    public static final String KEY_POSITION = "position";

    public static int SET = 0;
    public static int ADVANTAGE = 0;
    public static String FINAL_SET = "";
    public static int BALL_STATE = 1;
    public static String SURFACE = "";



}
