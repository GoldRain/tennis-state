package com.mingyue.pekostudio.tennisstats.model;

/**
 * Created by HugeRain on 6/23/2017.
 */

public class GameModel {

    int _id = 0;
    String _playerName1 = "";
    String _playerName2 = "";

    String _time = "";

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_playerName1() {
        return _playerName1;
    }

    public void set_playerName1(String _playerName1) {
        this._playerName1 = _playerName1;
    }

    public String get_playerName2() {
        return _playerName2;
    }

    public void set_playerName2(String _playerName2) {
        this._playerName2 = _playerName2;
    }

    public String get_time() {
        return _time;
    }

    public void set_time(String _time) {
        this._time = _time;
    }
}
