package com.mingyue.pekostudio.tennisstats.preference;

/**
 * Created by HGS on 12/11/2015.
 */
public class PrefConst {

    public static final String PREFKEY_USEREMAIL = "email";

    public static final String PREFKEY_LASTLOGINID = "lastlogin_id";

}
