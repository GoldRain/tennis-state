package com.mingyue.pekostudio.tennisstats.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.mingyue.pekostudio.tennisstats.R;
import com.mingyue.pekostudio.tennisstats.base.CommonActivity;
import com.mingyue.pekostudio.tennisstats.commons.Constants;

import static com.mingyue.pekostudio.tennisstats.commons.Constants.BALL_STATE;

public class SettingActivity extends CommonActivity implements View.OnClickListener {

    LinearLayout lyt_set,lyt_game_1set, lyt_advantage, lyt_final_set, lyt_surface;
    TextView txv_set, txv_game_set, txv_advantage, txv_final_set, txv_surface;
    ImageView imv_play1_ball, imv_play2_ball;
    EditText edt_player1_name, edt_player2_name, edt_place;

    FrameLayout fly_ball1, fly_ball2;
    //set alert
    ImageView imv_1set, imv_3set, imv_5set;
    //set advantage
    ImageView imv_advantage, imv_no_advantage, imv_semi_advantage;
    //set Final Set
    ImageView imv_tie_break, imv_long, imv_super_tb;
    //set surface
    ImageView imv_hard, imv_clay, imv_grass, imv_omni, imv_carpet;

    String[] strings = null;
    MaterialSpinner spinner;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        loadLayout();
    }

    private void loadLayout() {

        ImageView imv_back = (ImageView)findViewById(R.id.imv_back);
        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoMain();
            }
        });

        //Linear Layout functions setting
        lyt_set = (LinearLayout)findViewById(R.id.lyt_set);
        lyt_set.setOnClickListener(this);

        lyt_game_1set = (LinearLayout)findViewById(R.id.lyt_game_1set);
        lyt_game_1set.setOnClickListener(this);

        lyt_advantage = (LinearLayout)findViewById(R.id.lyt_advantage);
        lyt_advantage.setOnClickListener(this);

        lyt_final_set = (LinearLayout)findViewById(R.id.lyt_final_set);
        lyt_final_set.setOnClickListener(this);

        lyt_surface = (LinearLayout)findViewById(R.id.lyt_surface);
        lyt_surface.setOnClickListener(this);

        //set Text
        txv_set = (TextView)findViewById(R.id.txv_set);
        txv_set.setText(String.valueOf("1" + " " + "sets"));
        /*txv_game_set =(TextView)findViewById(R.id.txv_1set_game);*/
        txv_advantage =(TextView)findViewById(R.id.txv_advantage);
        txv_final_set =(TextView)findViewById(R.id.txv_final_set);
        txv_surface = (TextView)findViewById(R.id.txv_surface);

        //ball setting
        fly_ball1 = (FrameLayout)findViewById(R.id.fly_ball1);
        fly_ball1.setOnClickListener(this);

        fly_ball2 = (FrameLayout)findViewById(R.id.fly_ball2);
        fly_ball2.setOnClickListener(this);

        imv_play1_ball = (ImageView)findViewById(R.id.imv_play1_ball);
        imv_play1_ball.setOnClickListener(this);

        imv_play2_ball = (ImageView)findViewById(R.id.imv_play2_ball);
        imv_play2_ball.setOnClickListener(this);

        imv_play1_ball.setVisibility(View.VISIBLE);
        imv_play2_ball.setVisibility(View.GONE);

        //Player1 name, player2 name, place
        edt_player1_name = (EditText)findViewById(R.id.edt_player1_name);
        edt_player2_name =(EditText)findViewById(R.id.edt_player2_name);
        edt_place = (EditText)findViewById(R.id.edt_place);

        spinner = (MaterialSpinner)findViewById(R.id.spinner);

        strings = new String[]{"1 games", "2 games", "3 games", "4 games", "5 games", "6 games", "7 games", "8 games", "9 games", "10 game"};
        spinner.setItems(strings);
        final String[] producttype = {strings[0]};
        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                producttype[0] = item;

                Log.d("item===", item);

                //0: all: 1: request: 2:accepted:3:rejected

                if (item.equals("1 games")){


                } else if (item.equals("2 games")){


                } else if (item.equals("3 games")) {


                } else if (item.equals("4 games")) {


                } else if (item.equals("5 games")){


                } else if (item.equals("6 games")) {


                } else if (item.equals("7 games")) {

                } else if (item.equals("8 games")){


                } else if (item.equals("9 games")) {


                } else if (item.equals("10 games")) {

                }
            }
        });


        LinearLayout activity_setting = (LinearLayout)findViewById(R.id.activity_setting);
        activity_setting.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_player1_name.getWindowToken(),0);
                return false;
            }
        });

    }

    private void gotoMain(){

        startActivity(new Intent(SettingActivity.this, MainActivity.class));

        overridePendingTransition(0,0);
        finish();
    }

    private void selectSet(){

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_set);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));

        imv_1set = (ImageView)dialog.findViewById(R.id.imv_1set);
        imv_1set.setOnClickListener(this);

        imv_3set = (ImageView)dialog.findViewById(R.id.imv_3set);
        imv_3set.setOnClickListener(this);

        imv_5set = (ImageView)dialog.findViewById(R.id.imv_5set);
        imv_5set.setOnClickListener(this);

        selectSet(true, false, false);

        Button btn_next = (Button)dialog.findViewById(R.id.btn_ok);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Constants.SET == 1)txv_set.setText(String.valueOf(1) +" " + "sets");
                else if (Constants.SET == 3) txv_set.setText(String.valueOf(3)+" " + "sets");
                else if (Constants.SET == 5) txv_set.setText(String.valueOf(5)+" " + "sets");

                dialog.dismiss();
            }
        });

        dialog.show();

    }

    private void selectAdvantage(){

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_advantage);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));

        imv_advantage = (ImageView)dialog.findViewById(R.id.imv_advantage);
        imv_advantage.setOnClickListener(this);

        imv_no_advantage = (ImageView)dialog.findViewById(R.id.imv_no_advantage);
        imv_no_advantage.setOnClickListener(this);

        imv_semi_advantage = (ImageView)dialog.findViewById(R.id.imv_semi_advantage);
        imv_semi_advantage.setOnClickListener(this);

        selectAdvantage(true, false, false);

        Button btn_next = (Button)dialog.findViewById(R.id.btn_ok);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Constants.ADVANTAGE == 1)txv_advantage.setText("Advantage");
                else if (Constants.ADVANTAGE == 3) txv_advantage.setText("No Advantage");
                else if (Constants.ADVANTAGE == 5) txv_advantage.setText("Semi Advantage");

                dialog.dismiss();
            }
        });

        dialog.show();

    }

    private void selectFinalSet(){

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_final_set);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));

        imv_tie_break = (ImageView)dialog.findViewById(R.id.imv_tie_break);
        imv_tie_break.setOnClickListener(this);

        imv_long = (ImageView)dialog.findViewById(R.id.imv_long);
        imv_long.setOnClickListener(this);

        imv_super_tb = (ImageView)dialog.findViewById(R.id.imv_super_tb);
        imv_super_tb.setOnClickListener(this);

        selectFinalSet(true, false, false);

        Button btn_next = (Button)dialog.findViewById(R.id.btn_ok);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Constants.FINAL_SET.equals("Tie Break"))txv_final_set.setText(Constants.FINAL_SET);
                else if (Constants.FINAL_SET.equals("Long")) txv_final_set.setText(Constants.FINAL_SET);
                else if (Constants.FINAL_SET.equals("Super TB")) txv_final_set.setText(Constants.FINAL_SET);

                dialog.dismiss();
            }
        });

        dialog.show();

    }

    private void selectSurface(){

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_surface);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));

        imv_hard = (ImageView)dialog.findViewById(R.id.imv_hard);
        imv_hard.setOnClickListener(this);

        imv_clay = (ImageView)dialog.findViewById(R.id.imv_clay);
        imv_clay.setOnClickListener(this);

        imv_grass = (ImageView)dialog.findViewById(R.id.imv_grass);
        imv_grass.setOnClickListener(this);

        imv_omni = (ImageView)dialog.findViewById(R.id.imv_omni);
        imv_omni.setOnClickListener(this);

        imv_carpet = (ImageView)dialog.findViewById(R.id.imv_carpet);
        imv_carpet.setOnClickListener(this);

        selectSurface(true, false, false, false, false);

        Button btn_next = (Button)dialog.findViewById(R.id.btn_ok);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Constants.SURFACE.equals("Hard"))txv_surface.setText(Constants.SURFACE);
                else if (Constants.SURFACE.equals("Clay")) txv_surface.setText(Constants.SURFACE);
                else if (Constants.SURFACE.equals("Grass")) txv_surface.setText(Constants.SURFACE);
                else if (Constants.SURFACE.equals("Omni")) txv_surface.setText(Constants.SURFACE);
                else if (Constants.SURFACE.equals("Carpet")) txv_surface.setText(Constants.SURFACE);

                dialog.dismiss();
            }
        });

        dialog.show();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.lyt_set:
                selectSet();
                break;

            case R.id.lyt_advantage:
                selectAdvantage();
                break;

            case R.id.lyt_final_set:
                selectFinalSet();
                break;

            case R.id.lyt_surface:
                selectSurface();
                break;

            case R.id.fly_ball1:
                imv_play1_ball.setVisibility(View.VISIBLE);
                imv_play2_ball.setVisibility(View.GONE);
                BALL_STATE = 1;
                break;

            case R.id.fly_ball2:
                imv_play2_ball.setVisibility(View.VISIBLE);
                imv_play1_ball.setVisibility(View.GONE);
                BALL_STATE = 2;
                break;

            //set setting
            case R.id.imv_1set:
                selectSet(true, false, false);
                Constants.SET = 1;
                break;
            case R.id.imv_3set:
                selectSet(false, true, false);
                Constants.SET = 3;
                break;
            case R.id.imv_5set:
                selectSet(false, false, true);
                Constants.SET = 5;
                break;

            //advantage alert setting
            case R.id.imv_advantage:
                selectAdvantage(true, false, false);
                Constants.ADVANTAGE = 1;
                break;
            case R.id.imv_no_advantage:
                selectAdvantage(false, true,false);
                Constants.ADVANTAGE = 3;
                break;
            case R.id.imv_semi_advantage:
                selectAdvantage(false, false,true);
                Constants.ADVANTAGE = 5;
                break;

            //Final Set setting
            case R.id.imv_tie_break:
                selectFinalSet(true, false, false);
                Constants.FINAL_SET = "Tie Break";
                break;
            case R.id.imv_long:
                selectFinalSet(false, true, false);
                Constants.FINAL_SET = "Long";
                break;
            case R.id.imv_super_tb:
                selectFinalSet(false, false, true);
                Constants.FINAL_SET = "Super TB";
                break;

            //Surface setting
            case R.id.imv_hard:
                selectSurface(true, false, false, false, false);
                Constants.SURFACE = "Hard";
                break;
            case R.id.imv_clay:
                selectSurface(false, true, false, false, false);
                Constants.SURFACE = "Clay";
                break;
            case R.id.imv_grass:
                selectSurface(false, false, true, false, false);
                Constants.SURFACE = "Grass";
                break;
            case R.id.imv_omni:
                selectSurface(false, false, false, true, false);
                Constants.SURFACE = "Omni";
                break;
            case R.id.imv_carpet:
                selectSurface(false, false, false, false, true);
                Constants.SURFACE = "Carpet";
                break;



        }

    }

    private void selectSet(boolean a, boolean b, boolean c){
        imv_1set.setSelected(a);imv_3set.setSelected(b);imv_5set.setSelected(c);
    }

    private void selectAdvantage(boolean a, boolean b, boolean c){
        imv_advantage.setSelected(a); imv_no_advantage.setSelected(b);imv_semi_advantage.setSelected(c);
    }

    private void selectFinalSet(boolean a, boolean b, boolean c){
        imv_tie_break.setSelected(a); imv_long.setSelected(b);imv_super_tb.setSelected(c);
    }

    private void selectSurface(boolean a, boolean b, boolean c, boolean d, boolean e){
        imv_hard.setSelected(a); imv_clay.setSelected(b);imv_grass.setSelected(c);imv_omni.setSelected(d);imv_carpet.setSelected(e);
    }

    @Override
    public void onBackPressed() {
        gotoMain();
    }
}
