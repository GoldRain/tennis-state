package com.mingyue.pekostudio.tennisstats.activity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.mingyue.pekostudio.tennisstats.R;
import com.mingyue.pekostudio.tennisstats.base.CommonActivity;
import com.mingyue.pekostudio.tennisstats.commons.Constants;

public class IntroActivity extends CommonActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        gotoMain();
    }

    private void gotoMain() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                ActivityOptions options =
                        ActivityOptions.makeCustomAnimation(IntroActivity.this, R.anim.fade_in, R.anim.fade_out);

                Intent intent = new Intent(IntroActivity.this, MainActivity.class);

                startActivity(intent,options.toBundle());

                overridePendingTransition(0,0);

                finish();
            }
        }, Constants.SPLASH_TIME);
    }
}
